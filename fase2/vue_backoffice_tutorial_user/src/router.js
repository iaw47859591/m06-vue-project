import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/main",
      name: "main",
      component: () => import("./components/MainPage")
    },
    {
      path: "/tutorials",
      name: "tutorials",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/addTutorial",
      name: "addTutorial",
      component: () => import("./components/AddTutorial")
    },
    {
      path: "/users",
      name: "users",
      component: () => import("./components/UsersList")
    },
    {
      path: "/users/:id",
      name: "user-details",
      component: () => import("./components/User")
    },
    {
      path: "/addUser",
      name: "addUser",
      component: () => import("./components/AddUser")
    },
    {
      path: "/marketplace",
      name: "marketplace",
      component: () => import("./components/MarketPlace")
    },
    {
      path: "/donation/:id",
      name: "tutorial-donation",
      component: () => import("./components/Donation")
    },
    {
      path: "/news",
      name: "news",
      component: () => import("./components/News")
    },
  ]
});
