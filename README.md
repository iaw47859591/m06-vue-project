# m06-vue-project

Creació d'un front del backend per fer manteniment de productes

# Instal·lació

El projecte per l'exàmen es troba a examenVue

## NPM

Primer ens hem d'assegurar que tinguem el paquet npm instal·lat.  
`$ node -v`  
`$ npm -v`

Si no reben cap versió l'instalarem:  
`$ dnf install -y nodejs`

I tornarem a fer els passos anteriors per assegurar que la instal·lació es correcta.


## VUE/CLI && Webpack

Per instal·lar vue/CLI  
`$ npm update -g @vue/cli`

Per instal·lar Webpack  
`$ npm install --save-dev webpack`

## Dintre del projecte
### Dependencies

Per instal·lar les dependències del porjecte:  
`$ npm install`

### Iniciar porjecte

Podem iniciar el projecte amb la següent comanda:  
`$ npm run serve`

## FakeServer
Assegurar que ja sigui instal·lat:  
`$ json-server -v`

Instal·lar:
`$ npm install -g json-server`  

### Iniciar servidor
Al directori on es troba l'archiu `db.json`:  
`$ json-server --watch db.json`.
