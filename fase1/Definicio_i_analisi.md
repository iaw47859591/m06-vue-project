# Definició i analisis

## Manera de treballar
Treballaré individualment.  
El framework a usar serà VUE amb un projecte VUE-CLI.  
El control de versions estarà gestionat via GIT a través de la plataforma ***gitlab***  

## Descripció del Backend  
M'agradaria basar-me en el projecte ABP, ja que aixì podria incorporar el treball realitzat a aquest.  

El backend del projecte APB té ususaris i tutorials.  

Els usuaris poden ser estudiants, professors o administradors.  

### Items
- **Usuari**: Nom d'usuari, contrassenya(encriptada?), email.
- **Tutorial**: Titol, tecnologies, professor, contingut(a la taula es veuria només un tros d'aquest), comentaris

### Usuaris i gestió
- **Estudiant**: No té acces al CRUD  

- **Professor**: Pot administrar tots els seus tutorials (titol, tencologies i contingut).  
No podrà modificar l'apartat professor ja que causaria problemes.  
Dintre els seus tutorials fora interessant poder gestionar els comentaris d'altres usuaris en aquests.  
També podria incorporar una alerta als comentaris per a poder denunciar un usuari (això faria arribar una alerta al administrador).  

- **Administrador**: Administrar tots els tutorials i tots els usuaris.  
Als usuaris estudiants podria donar-los modificar-los per ser professors o donar-los de baixa, per exemple, per mal comportament.  
Respecte els tutorials pot realitzar les mateixes accions que un professor pero a tots els tutorials.  


### Interaccions
Tots els items tindran botons de **update** i **delete**.  
Per **crear** podria haver un formulari a la mateixa finestra o un botó que obriria una finestra pop-up amb el corresponent formulari. Crec que la segona opció és més idònia.  
Els tutorials i els usuaris es podràn **filtrar** i **orderar** mitjançant un formulari o una barra de cerca.  
Per modificar el contingut dels tutorials s'hauria de generar-se un botó, ja que s'hauria de generar una finestra especial.  
Forà interessant afegir un botó "**ensenya**" que enllaçaria al portal original per accedir a la vista d'usuari o de tutorial.  


## GIT  
[Enllaç](https://gitlab.com/iaw47859591/m06-vue-project)

### Bona gestió
Sempre es treballarà en branques per no afectar a la branca master.  
Els ***merge*** es realitzaràn un cop estigui enllestida una funció operativa.  
Es realitzaràn 3 ***Releases***:  
- CRUD de tutorials.
- CRUD d'usuaris.
- General.

